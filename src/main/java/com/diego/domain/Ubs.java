package com.diego.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.diego.domain.enums.ScoreUbs;

import lombok.Data;

@Entity
@Data
public class Ubs implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	private String name;
	private String address;
	private String city;
	private String phone;
	private double lati;
	private double longi;
	private Integer size;
	private Integer adaptation_for_seniors;
	private Integer medical_equipment;
	private Integer medicine;
	
	public Ubs() {
	}

	public Ubs(Integer id, String name, String adress, String city, String phone,double lati,double longi,
			ScoreUbs size, ScoreUbs adaptation_for_seniors, ScoreUbs medical_equipment, ScoreUbs medicine) {
		super();
		this.id = id;
		this.name = name;
		this.address = adress;
		this.city = city;
		this.phone = phone;
		this.lati = lati;
		this.longi = longi;
		this.size = size.getCod();
		this.adaptation_for_seniors = adaptation_for_seniors.getCod();
		this.medical_equipment = medical_equipment.getCod();
		this.medicine = medicine.getCod();

	}

	
}
