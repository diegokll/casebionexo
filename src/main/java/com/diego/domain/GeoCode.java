package com.diego.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class GeoCode implements Serializable{

	private static final long serialVersionUID = 1L;

	private double lati;
	private double longi;
	
	public GeoCode() {
	}

	public GeoCode(double lati, double longi) {
		super();
		this.lati = lati;
		this.longi = longi;
	}


	
}
