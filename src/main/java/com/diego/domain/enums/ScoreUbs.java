package com.diego.domain.enums;

public enum ScoreUbs {

	
	ABAIXO_DA_MEDIA(1, "Desempenho mediano ou  um pouco abaixo da média"),
	ACIMA_DA_MEDIA(2, "Desempenho acima da média"),
	MUITO_ACIMA_DA_MEDIA(3,"Desempenho muito acima da média");
	
	private int cod;
	private String descricao;
	
	private ScoreUbs(int cod, String descricao){
		this.cod = cod;
		this.descricao = descricao;
	}

	public int getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public static ScoreUbs toEnum(Integer cod) {
		if(cod==null) {
			return null;
		}
		
		for(ScoreUbs x : ScoreUbs.values()) {
			if(cod.equals(x.getCod())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id Inválido: "+cod);
	}
}
