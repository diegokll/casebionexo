package com.diego.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class Scores implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer size;
	private Integer adaptation_for_seniors;
	private Integer medical_equipment;
	private Integer medicine;
	
	public Scores() {
	}

	public Scores(Integer size, Integer adaptation_for_seniors, Integer medical_equipment, Integer medicine) {
		super();
		this.size = size;
		this.adaptation_for_seniors = adaptation_for_seniors;
		this.medical_equipment = medical_equipment;
		this.medicine = medicine;
	}

}
