package com.diego.dto;

import java.io.Serializable;

import com.diego.domain.GeoCode;
import com.diego.domain.Scores;
import com.diego.domain.Ubs;

import lombok.Data;
@Data
public class UbsDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String address;
	private String city;
	private String phone;
	private GeoCode geoCode;
	private Scores scores;
	
	public UbsDTO() {
	}

	public UbsDTO(Ubs ubs) {
		super();
		this.id = ubs.getId();
		this.name = ubs.getName();
		this.address = ubs.getAddress();
		this.city = ubs.getCity();
		this.phone = ubs.getPhone();
		this.geoCode = new GeoCode(ubs.getLati(), ubs.getLongi());
		this.scores = new Scores(ubs.getSize(), ubs.getAdaptation_for_seniors(), ubs.getMedical_equipment(), ubs.getMedicine());
	}
	
}
