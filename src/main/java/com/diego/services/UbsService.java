package com.diego.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.diego.domain.Ubs;
import com.diego.repositories.UbsRepository;

@Service
public class UbsService {
	
	@Autowired
	private UbsRepository repo;

	public Page<Ubs> findByGeoCod(Integer page,String query, Integer per_page, String orderBy,String direction){		
		PageRequest pageRequest = PageRequest.of(page,per_page, Direction.valueOf(direction),orderBy);
		String coord[]=query.split(",");
		return repo.findByGeoCod(coord[0],coord[1],pageRequest);	
	}
	
	
}
