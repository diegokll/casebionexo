package com.diego.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diego.domain.Ubs;
import com.diego.domain.enums.ScoreUbs;
import com.diego.repositories.UbsRepository;

@Service
public class DBService {

	@Autowired
	private UbsRepository usbRepository;
	
	public void instantiateTestDataBase() {

		Ubs ubs1 = new Ubs(3492,"US OSWALDO DE SOUZA,TV ADALTO BOTELHO", "GETULIO VARGAS", "Aracaju", "7931791326",-10.9112370014188, -37.0620775222768 ,ScoreUbs.ACIMA_DA_MEDIA, ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA);
	
		Ubs ubs2 = new Ubs(6749836,"UAPSF PROF HETTY ROSA DE MOURA E COSTA", "AVENIDA EZIDIO NEVES", "Doutor Ulysses", "4136641208", -23.6099946498864, -46.7057347297655, ScoreUbs.ABAIXO_DA_MEDIA, ScoreUbs.ABAIXO_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA);

		Ubs ubs3 = new Ubs(39860, "UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO", "RUA BARAO MELGACO", "São Paulo", "1137582329", -23.6099946498864, -46.7057347297655,ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.ABAIXO_DA_MEDIA, ScoreUbs.MUITO_ACIMA_DA_MEDIA);
		
		Ubs ubs4 = new Ubs(39865, "UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO", "RUA BARAO MELGACO", "São Paulo", "1137582329", -15, -10, ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA);

		Ubs ubs5 = new Ubs(39869, "UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO", "RUA BARAO MELGACO", "São Paulo", "1137582329", -10, -37, ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.MUITO_ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA, ScoreUbs.ACIMA_DA_MEDIA);
	
		usbRepository.saveAll(Arrays.asList(ubs1,ubs2,ubs3,ubs4,ubs5));
		
	}
	
}
