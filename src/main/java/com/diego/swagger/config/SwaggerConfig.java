package com.diego.swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	@Bean
	public Docket CaseApi() {

		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.diego"))
				.paths(PathSelectors.ant("/**"))
				.build().apiInfo(apiInfo());
		
	}
	
	private ApiInfo apiInfo() {
		
		ApiInfo apiInfo = new ApiInfoBuilder()
	            .title ("Aplicação Web API,")
	            .description ("Essa uma API para localização de Unidades de saúde próximas baseado em coordenadas de latitude e longitude."
	            		+ "\n Abaixo constam as informações das classes do projeto."
	            		+ "\n É possível serem feitos testes de requisição GET.")
	            .contact(new Contact("Diego Henrique Dantas Marques", 
	            					 "https://www.linkedin.com/in/diego-marquess/",
	            					 "diegohenriquedantasmarquess@gmail.com"))
	            .license("Apache License Version 2.0")
	            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
	            .version("1.0.0")
	            .build();

	    return apiInfo;
	}
	
}


