package com.diego.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.diego.domain.Ubs;
import com.diego.dto.UbsDTO;
import com.diego.services.UbsService;

@RestController
@RequestMapping(value="/api/v1")
public class UbsResource {
	
	@Autowired
	UbsService service;
	
	@RequestMapping(value = "/find_ubs", method=RequestMethod.GET)
	public ResponseEntity<Page<UbsDTO>> findByGeoCod(
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "query", defaultValue = "0") String query,
			@RequestParam(value = "per_page", defaultValue = "10")Integer per_page,
			@RequestParam(value = "orderBy", defaultValue = "id")String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC")String direction) {
		Page<Ubs> list = service.findByGeoCod(page,query,per_page, orderBy, direction);
		Page<UbsDTO> listDto = list.map(obj -> new UbsDTO(obj));
		return ResponseEntity.ok().body(listDto);
	}
}
