package com.diego.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.diego.domain.Ubs;

@Repository
public interface UbsRepository extends JpaRepository<Ubs, Integer>{

	int dist=5;

	  @Query(value = "(select * from (\r\n" + 
		"SELECT UBS.*, round( 6371 * (ACOS(COS((( 90 - ?1) * "+Math.PI+")/180) *\r\n" + 
		" COS((( 90 - UBS.LATI) * "+Math.PI+")/180) + SIN( (( 90 - ?1) * "+Math.PI+")/180 ) * \r\n" + 
		"SIN( (( 90 - UBS.LATI) * "+Math.PI+")/180 ) * COS( (( ?2- UBS.LONGI) * \r\n" + 
		""+Math.PI+")/180 ) ) ) * 1.15 ,2) as dist FROM UBS) result where result.dist <="+dist+")",nativeQuery = true)
	  public Page<Ubs> findByGeoCod(String coord1, String coord2,Pageable pageable);
	  
	  
}
